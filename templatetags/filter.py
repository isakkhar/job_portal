import datetime
from django import template
from django.utils.datetime_safe import date

register = template.Library()


@register.filter(name="my_filter_name")
def my_filter_name(input):
    output = do_something_with_input(input)
    return date


register.filter('my_filter_name', my_filter_name)